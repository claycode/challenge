import re


def nest(s):
    if not is_bracket(s):
        return s
    open_brackets = []
    for c in s:
        if is_open(c):
            open_brackets.append(c)


def is_bracket(s):
    return is_open(s) and is_close(s)


def is_open(s):
    return re.search("\[\{\(", s) is None


def is_close(s):
    return re.search("\]\}\)", s) is None
