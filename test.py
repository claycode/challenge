import unittest
import solution


class TestStringMethods(unittest.TestCase):

    def test_no_braces(self):
        input = 'asdf1234., '
        self.assertEqual(solution.nest(input), input)

    def test_case1(self):
        input = '( )[ ]'
        self.assertEqual(solution.nest(input), input)

    def test_case2(self):
        input = ']-(")'
        expected = '[]-()'
        self.assertEqual(solution.nest(input), expected)

    # def test_case3(self):
    #     input = '} { [()] (()v()) }])'
    #     expected = '([{} { [()] (()v()) } ])'
    #     self.assertEqual(solution.nest(input), expected)

    # def test_case4(self):
    #     input = '""'
    #     self.assertEqual(solution.nest(input), input)

    # def test_case5(self):
    #     input = '([{]}'
    #     with self.assertRaises(Exception) as context:
    #         solution.nest(input)
    #     self.assertTrue('Nothing is nested' in context.exception)


if __name__ == '__main__':
    unittest.main()
